﻿function checkLogged($location) {
    if (getCookie("username") != "seti") {
        $location.path('/');
    }
}

function PatientLookupController($scope, $location, $http) {
    checkLogged($location);
    $scope.findpatient = function () {
        var httpurl = "/patientid/" + $scope.PatientFirst + "/" + $scope.PatientLast;
        $http({ method: 'GET', url: httpurl }).success(function (data, status) {
            $scope.statusPatientID = status;
            $scope.dataPatientID = data;
        }).
      error(function (data, status) {
          $scope.data = data || "Request failed";
          alert("loading of patient failed");
          $scope.status = status;
      });
    };
}

function PatientFormController($scope, $http, $templateCache, $filter, $routeParams, $location) {
    checkLogged($location);
    var httpurl = "/getpatientinfo/" + $routeParams.patientID;
    $scope.mypatientid = $routeParams.patientID;
    $scope.newDate = Date();
    $http({ method: 'GET', url: httpurl }).success(function (data, status) {
        $scope.statusPatientInfo = status;
        $scope.dataPatientInfo = data;
        $scope.convertDOB();
    }).
      error(function (data, status) {
          $scope.data = data || "Request failed";
          $scope.status = status;
      });

    $scope.convertDOB = function () {
        var dateconvert = $scope.dataPatientInfo[0].dateBirth.substring(6, 23);
        // console.log("mydate: " + dateconvert.toString());
        var mydate = parseInt(dateconvert);
        $scope.dataPatientInfo[0].dateBirth = $filter('date')(new Date(mydate), 'MM/dd/y');
    }

    $scope.updatepatientinfo = function () {
        mydata = '{ "PatientItem":' + JSON.stringify($scope.dataPatientInfo[0]) + '}';
        $http({
            method: 'Post',
            url: "/updatepatientinfo",
            data: mydata
        }).success(function (data, status) {
            $scope.statusPost = status;
            $scope.dataPost = data;
            alert("Patient Info Updated");
            var myfields = document.getElementsByClassName('patient-item');
            for (i = 0; i < myfields.length; i++) {
                myfields[i].value = '';
            }
        }).
      error(function (data, status) {
          $scope.data = data || "Request failed";
          alert("Failed to Update Patient");
          $scope.status = status;
      });
    };
}

function AssessmentsDetailController($scope, $http, $location, $templateCache, $filter, $routeParams) {
    checkLogged($location);
    var httpurl = "/assessmentsdetail/" + $routeParams.patientID + "/" + $routeParams.assessmentName;
    $scope.mypatientid = $routeParams.patientID;
    $http({ method: 'GET', url: httpurl }).success(function (data, status) {
        $scope.statusAssessmentsAnswers = status;
        $scope.dataAssessmentsAnswers = data;
        $scope.convertDate();
    }).
      error(function (data, status) {
          $scope.data = data || "Request failed";
          $scope.status = status;
      });

    $scope.convertDate = function () {
        console.log($scope.dataAssessmentsAnswers.length);
        var dateconvert = "";
        var mydate = 0;
        for (i = 0; i < $scope.dataAssessmentsAnswers.length; i++) {
            dateconvert = $scope.dataAssessmentsAnswers[i].createDate.substring(6, 23);
            // console.log("mydate: " + dateconvert.toString());
            mydate = parseInt(dateconvert);
            $scope.dataAssessmentsAnswers[i].createDate = $filter('date')(new Date(mydate), 'MM/dd/y');
        }
    }
}

function AssessmentsCompletedController($scope, $http, $templateCache, $filter, $routeParams, $location) {
    checkLogged($location);
    var httpurl = "/assessmentscompleted/" + $routeParams.patientID;
    $scope.mypatientid = $routeParams.patientID;
    $http({ method: 'GET', url: httpurl }).success(function (data, status) {
        $scope.statusAssessmentsCompleted = status;
        $scope.dataAssessmentsCompleted = data;
    }).
      error(function (data, status) {
          $scope.data = data || "Request failed";
          $scope.status = status;
      });
}

function AssessmentsFormController($scope, $http, $templateCache, $filter, $routeParams, $location) {
    checkLogged($location);
    $scope.mypatientid = $routeParams.patientID;
    var httpurl = '/assessmentlist';
    $http({ method: 'GET', url: httpurl, cache: $templateCache }).success(function (data, status) {
        $scope.statusAssessmentList = status;
        $scope.dataAssessmentList = data;
    }).
      error(function (data, status) {
          $scope.data = data || "Request failed";
          $scope.status = status;
      });

    $scope.submitButtonState = 0;

    $scope.getSubmitButton = function () {
        return $scope.submitButtonState;
    }


    $scope.assessmentdropdowns = function (fieldID) {
        var httpurl = "/assessmentdropdowns/" + fieldID;

        $scope.dataDropdowns = [];
        $http({ method: 'GET', url: httpurl }).success(function (data, status) {
            $scope.statusDropdowns = status;
            $scope.dataDropdowns = data;

        }).
      error(function (data, status) {
          $scope.data = data || "Request failed";
          alert("loading of dropdowns failed");
          $scope.status = status;
      });
    };

    $scope.getassessmentform = function () {
        var httpurl = "/assessmentfieldload/" + $scope.selectedPatientAssessment;
        $scope.submitButtonState = 1;

        $http({ method: 'GET', url: httpurl }).success(function (data, status) {
            $scope.statusAssessment = status;
            $scope.dataAssessment = data;
            for (i = 0; i < data.length; i++) {
                $scope.assessmentdropdowns(data[i].fieldID);
            }
            $scope.assessmentanswerspanel();
        }).
      error(function (data, status) {
          $scope.data = data || "Request failed";
          $scope.status = status;
      });

    };

    $scope.postassessmentform = function () {
        var mydata = $('#assessmentform').serializeArray();
        mydata = JSON.stringify(mydata);
        // console.log(mydata);
        mydata = '{ "Items":' + mydata + '}';

        $http({
            method: 'Post',
            url: "/assessmentpost",
            data: jQuery.parseJSON(mydata)
        }).success(function (data, status) {
            $scope.statusPost = status;
            $scope.dataPost = data;
            alert("Asessment posted, use this same form to add another entry to this assessment.");
            $scope.assessmentanswerspanel();
            var myfields = document.getElementsByClassName('assess-item');
            for (i = 0; i < myfields.length; i++) {
                myfields[i].value = '';
                $(":checkbox").attr("checked", false);
            }
        }).
      error(function (data, status) {
          $scope.data = data || "Request failed";
          alert("Assessment post failed!");
          $scope.status = status;
      });
    };

    $scope.assessmentanswerspanel = function () {
        var httpurl = "/assessmentsdetail/" + $scope.mypatientid + "/" + $scope.selectedPatientAssessment;
        $http({ method: 'GET', url: httpurl }).success(function (data, status) {
            $scope.statusAssessmentsAnswers = status;
            $scope.dataAssessmentsAnswers = data;
            $scope.convertDate();
        }).
      error(function (data, status) {
          $scope.data = data || "Request failed";
          $scope.status = status;
      });
    };

    $scope.convertDate = function () {
        console.log($scope.dataAssessmentsAnswers.length);
        var dateconvert = "";
        var mydate = 0;
        for (i = 0; i < $scope.dataAssessmentsAnswers.length; i++) {
            dateconvert = $scope.dataAssessmentsAnswers[i].createDate.substring(6, 23);
            // console.log("mydate: " + dateconvert.toString());
            mydate = parseInt(dateconvert);
            $scope.dataAssessmentsAnswers[i].createDate = $filter('date')(new Date(mydate), 'MM/dd/y');
        }
    }
}

function LoginController($scope, $http, $location) {
    $scope.submitlogin = function () {
        mydata = '{ "LoginItems":' + JSON.stringify($scope.user) + '}';
        console.log(mydata);
        $http({
            method: 'Post',
            url: "/assessmentlogin",
            data: mydata
        }).success(function (data, status) {
            setCookie("username", "seti", 10);
            $scope.statusPost = status;
            $scope.dataPost = data;
            $location.path('/patientlookup');
        }).
      error(function (data, status) {
          $scope.data = data || "Request failed";
          alert("Failed to Login");
          $scope.status = status;
      });
    }
}

function LogoutController($location) {
    setCookie("username", "", 10);
    $location.path('/');
}