﻿var assessmentsengine = angular.module('AssessmentsEngine', []);

assessmentsengine.controller('FetchCtrl', function ($scope, $http, $templateCache, $filter) {
    $scope.newDate = Date();
    $scope.getassessments = function () {
        $http({ method: 'GET', url: '/assessmentlist', cache: $templateCache }).success(function (data, status) {
            $scope.statusAssessmentList = status;
            $scope.dataAssessmentList = data;
        }).
      error(function (data, status) {
          $scope.data = data || "Request failed";
          $scope.status = status;
      });
    };

    $scope.findpatient = function () {
        var myurl = "/patientid/" + $scope.PatientFirst + "/" + $scope.PatientLast;
        $http({ method: 'GET', url: myurl }).success(function (data, status) {
            $scope.statusPatientID = status;
            $scope.dataPatientID = data;
            $scope.getassessments();
        }).
      error(function (data, status) {
          $scope.data = data || "Request failed";
          alert("loading of patient failed");
          $scope.status = status;
      });
    };

    $scope.getpatientinfo = function (patientID) {
        var myurl = "/getpatientinfo/" + patientID;
        $http({ method: 'GET', url: myurl }).success(function (data, status) {
            $scope.statusPatientInfo = status;
            $scope.dataPatientInfo = data;
            $scope.convertDOB();
        }).
      error(function (data, status) {
          $scope.data = data || "Request failed";
          $scope.status = status;
      });
    };
    $scope.convertDOB = function () {
        var dateconvert = $scope.dataPatientInfo[0].dateBirth.substring(6, 23);
        console.log("mydate: " + dateconvert.toString());
        var mydate = parseInt(dateconvert);
        $scope.dataPatientInfo[0].dateBirth = $filter('date')(new Date(mydate), 'MM/dd/y');
    }
    $scope.assessmentscompleted = function (patientID) {
        var myurl = "/assessmentscompleted/" + patientID;
        $http({ method: 'GET', url: myurl }).success(function (data, status) {
            $scope.statusAssessmentsCompleted = status;
            $scope.dataAssessmentsCompleted = data;
        }).
      error(function (data, status) {
          $scope.data = data || "Request failed";
          $scope.status = status;
      });
    };

    $scope.assessmentdropdowns = function (fieldID) {
        var myurl = "/assessmentdropdowns/" + fieldID;

        $http({ method: 'GET', url: myurl }).success(function (data, status) {
            $scope.statusDropdowns = status;
            $scope.dataDropdowns = data;
            $scope.getassessments();
        }).
      error(function (data, status) {
          $scope.data = data || "Request failed";
          alert("loading of dropdowns failed");
          $scope.status = status;
      });
    };

    $scope.getassessmentform = function () {

        var myurl = "/assessmentfieldload/" + $scope.selectedPatientAssessment;
        $http({ method: 'GET', url: myurl }).success(function (data, status) {
            $scope.statusAssessment = status;
            $scope.dataAssessment = data;
            $scope.assessmentdropdowns(data[0].fieldID);
        }).
      error(function (data, status) {
          $scope.data = data || "Request failed";
          $scope.status = status;
      });

    };

    $scope.postassessmentform = function () {
        var mydata = $('#assessmentform').serializeArray();
        mydata = JSON.stringify(mydata);
        mydata = '{ "Items":' + mydata + '}';
        $http({
            method: 'Post',
            url: "/assessmentpost",
            data: jQuery.parseJSON(mydata)
        }).success(function (data, status) {
            $scope.statusPost = status;
            $scope.dataPost = data;
            alert("Asessment Posted!");
            $scope.getassessments();
        }).
      error(function (data, status) {
          $scope.data = data || "Request failed";
          alert("Assessment failed");
          $scope.status = status;
      });
    };

    $scope.updatepatientinfo = function () {
        mydata = '{ "PatientItem":' + JSON.stringify($scope.dataPatientInfo[0]) + '}';
        $http({
            method: 'Post',
            url: "/updatepatientinfo",
            data: mydata
        }).success(function (data, status) {
            $scope.statusPost = status;
            $scope.dataPost = data;
            alert("post worked");
            $scope.getassessments();
        }).
      error(function (data, status) {
          $scope.data = data || "Request failed";
          alert("Post failed");
          $scope.status = status;
      });
    };
});

