using System;
using System.Linq;
using System.Configuration;
using System.Collections.Generic;
using ServiceStack.Configuration;
using ServiceStack.OrmLite;
using ServiceStack.Common;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.Auth;
using ServiceStack.ServiceInterface.ServiceModel;
using ServiceStack.WebHost.Endpoints;
using BusinessObjectLayer.Entities.ClinicalAssessment;
using System.Text.RegularExpressions;
using AssessmentServiceStack.App_Start;
using System.Collections;

namespace AssessmentServiceStack
{
	//Request DTO
	public class AssessmentsList 
    {
       
    }

    public class AssessmentFieldLoad
    {
        public string fieldname {get; set;}
    }
        
    public class PatientID
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
    }
        
    public class AssessmentPost : IReturn<Boolean>
    {
        public List<AssessmentPostItem> Items { get; set; }

        public IEnumerator GetEnumerator()
        {
            foreach (AssessmentPostItem Item in Items)
            {
                yield return Item;

            }
        }
    }

    public class AssessmentsCompleted
    {
        public int patientid {get; set;}
    }

    public class GetPatientInfo
    {
        public int patientid { get; set; }
    }

    public class UpdatePatientInfo : IReturn<Boolean>
    {
        public PatientUpdateItem PatientItem { get; set; }
    }

    public class PatientUpdateItem
    {
        public int patientID { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postalCode { get; set; }
        public string country { get; set; }
        public string phoneHome { get; set; }
        public string phoneCell { get; set; }
        public int gender { get; set; }
        public DateTime dateBirth { get; set; }
        public string ssNumber { get; set; }
        public int ethnicity { get; set; }
    }

    public class AssessmentPostItem
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class AssessmentDropdowns
    {
        public int fieldid { get; set; }
    }

    public class AssessmentPostResponse
    {
        public string Result { get; set; }
        public ResponseStatus ResponseStatus { get; set; } //Where Exceptions get auto-serialized
    }

    public class GetPatientInfoResponse
    {
        public string Result { get; set; }
        public ResponseStatus ResponseStatus { get; set; } //Where Exceptions get auto-serialized
    }

    public class UpdatePatientInfoResponse
    {
        public string Result { get; set; }
        public ResponseStatus ResponseStatus { get; set; } //Where Exceptions get auto-serialized
    }

    public class AssessmentDropdownsResponse
    {
        public string Result { get; set; }
        public ResponseStatus ResponseStatus { get; set; } //Where Exceptions get auto-serialized
    }

    //Response DTO
    public class AssessmentsListResponse
    {
        public string Result { get; set; }
        public ResponseStatus ResponseStatus { get; set; } //Where Exceptions get auto-serialized
    }

    public class AssessmentsCompletedResponse
    {
        public string Result { get; set; }
        public ResponseStatus ResponseStatus { get; set; } //Where Exceptions get auto-serialized
    }

    public class AssessmentFieldLoadResponse
    {
        public string Result { get; set; }
        public ResponseStatus ResponseStatus { get; set; } //Where Exceptions get auto-serialized
    }

    public class PatientIDResponse
    {
        public string Result { get; set; }
        public ResponseStatus ResponseStatus { get; set; } //Where Exceptions get auto-serialized
    }

	public class AssessmentsService : Service
	{
        private string connString = "ClinicalAssessmentQA";

        public object Get(AssessmentsList request)
		{
            return Assessment.casAssessmentLoad(connString);
        }

        public object Get(AssessmentDropdowns request)
        {
            return Assessment.casDropdownList(request.fieldid, connString);
        }

        public object Get(PatientID request)
        {
            return Patient.casPatientInfoGet(request.firstname, request.lastname, connString);
        }

        public object Get(AssessmentsCompleted request)
        {
            return Assessment.casPatientAssementCompleteList(request.patientid, connString);
        }

        public object Get(GetPatientInfo request)
        {
            var info =
             Patient.casPatientInfoGet(request.patientid, connString);
            return info;
        }

        public object Get(AssessmentFieldLoad request)
        {
            return Assessment.casAssessmentFieldLoad(request.fieldname, connString);
        }

        private string[] dateFormatList = { "ffff", "yyyy-MM-dd", "yy-MM-dd", "MM/dd/yy", "M/d/yy", "MM/dd/yyyy", "MM-dd-yy", "MM-dd-yyyy", "yyyy/M/d" };

        public DateTime? GetDate(string date, string dateFormat, bool iterate)
        {
            DateTime? convertDate = null;
            try
            {
                convertDate = DateTime.ParseExact(date, dateFormat, null);
            }
            catch (FormatException exception)
            {
                if (iterate)
                {
                    foreach (string dFormat in dateFormatList)
                    {
                        convertDate = GetDate(date, dFormat, false);
                        if (convertDate != null) break;
                    }
                }
            }

            return convertDate;

        }

        public static DateTime ConvertMyDate(string convertDate)
        {
            //10342332000000700
            //1034233200000
            string parsedDate = Regex.Replace(convertDate, @"[^\d]", "");
            parsedDate = parsedDate.Remove(parsedDate.Length - 4);
            long convertDateTo = Convert.ToInt64(parsedDate);
            
            var ret = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            ret.AddMilliseconds(convertDateTo);
            return ret;
        }
    
        public object Post(UpdatePatientInfo request)
        {
            Patient myUpdatePatient = new Patient();
            var myPatient = request.PatientItem;
            DateTime patientDOB = myPatient.dateBirth;
            myUpdatePatient.Address1 = myPatient.address1;
            myUpdatePatient.Address2 = myPatient.address2;
            myUpdatePatient.Address3 = myPatient.address3;
            myUpdatePatient.City = myPatient.city;
            myUpdatePatient.Country = myPatient.country;
            myUpdatePatient.DateBirth = patientDOB;
            myUpdatePatient.Ethnicity = myPatient.ethnicity;
            myUpdatePatient.FirstName = myPatient.firstName;
            myUpdatePatient.Gender = myPatient.gender;
            myUpdatePatient.LastName = myPatient.lastName;
            myUpdatePatient.PatientID = myPatient.patientID;
            myUpdatePatient.PhoneCell = myPatient.phoneCell;
            myUpdatePatient.PhoneHome = myPatient.phoneHome;
            myUpdatePatient.PostalCode = myPatient.postalCode;
            myUpdatePatient.SSNumber = myPatient.ssNumber;
            myUpdatePatient.State = myPatient.state;
            return Patient.casPatientInfoUpdate(myUpdatePatient, connString);
        }

        public object Post(AssessmentPost request)
        {
            int mypatientid;
            int myfieldtype;
            int myfieldid;

            foreach (AssessmentPostItem myitem in request)
            {
                string[] mykeys = Regex.Split(myitem.name, "-");
                mypatientid = Convert.ToInt32(mykeys[0]);
                myfieldtype = Convert.ToInt32(mykeys[1]);
                myfieldid = Convert.ToInt32(mykeys[2]);

                if ((myfieldtype == 1) || (myfieldtype == 109))
                {
                    Assessment.casAssessmentFieldSave(mypatientid, myfieldid, null, Convert.ToInt32(myitem.value), null, null, connString);
                }
                
                if ((myfieldtype == 38) || (myfieldtype == 39))
                {
                    Assessment.casAssessmentFieldSave(mypatientid, myfieldid, Convert.ToString(myitem.value), null, null, null, connString);
                }
                
                if (myfieldtype == 111)
                {
                    Assessment.casAssessmentFieldSave(mypatientid, myfieldid, null, null, (DateTime) GetDate(myitem.value, dateFormatList[0], true), null, connString);
                }

                if (myfieldtype == 50)
                {
                    Boolean fieldState = new Boolean();
                    if (myitem.value == "true")
                    {
                        fieldState = true;
                    }
                    else
                    {
                        fieldState = false;
                    }

                    Assessment.casAssessmentFieldSave(mypatientid, myfieldid, null, null, null, fieldState, connString);
                }
            }  
            
            return true;
        }
	}
}
